<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Admin',
                'email' => 'admin@test.com',
                'password' => bcrypt('1234567890'),

            ],
            [
                'name' => 'Author',
                'email' => 'author@test.com',
                'password' => bcrypt('1234567890'),

            ],
        ];

        DB::table('users')->insert($users);
    }
}
