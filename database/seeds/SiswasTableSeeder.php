<?php

use Illuminate\Database\Seeder;

class SiswasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $siswas=[
            [
                'nama'   => 'Astrid',
                'alamat' => 'Medan',
                'nis'    => '10511',
            ],
            [
                'nama'   => 'Dini',
                'alamat' => 'Siantar',
                'nis'    => '10512',
            ],
        ];
        DB::table('siswas')->insert($siswas);
    }
}
