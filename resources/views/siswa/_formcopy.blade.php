<div class="form-group">
    <label for="squareInput">Nama</label>

    {!! Form::text('nama',null,['class'=>$errors->has('nama') ? 'form-control is-invalid' : 'form-control','required','autofocus','placeholder'=>'nama']) !!}
    @if ($errors->has('nama'))
    <span class="invalid-feedback">
        <strong>{{ $errors->first('nama') }}</strong>
    </span>
   @endif
</div>
<div class="form-group">
      <label for="squareInput">Alamat</label>
      {!! Form::textarea('alamat',null,['class'=>$errors->has('alamat') ? 'form-control is-invalid' : 'form-control','required','autofocus','placeholder'=>'alamat']) !!}
      @if ($errors->has('alamat'))
      <span class="invalid-feedback">
          <strong>{{ $errors->first('alamat') }}</strong>
      </span>
     @endif
</div>
<div class="form-group">
      <label for="squareInput">Nis</label>
     {!! Form::number('nis',null,['class'=>$errors->has('nis') ? 'form-control is-invalid' : 'form-control','required','autofocus','placeholder'=>'nis']) !!}
      @if ($errors->has('nis'))
      <span class="invalid-feedback">
          <strong>{{ $errors->first('nis') }}</strong>
      </span>
     @endif
</div>

@section('assets-bottom')
<script src="{{ asset('vendor/laravel-filemanager/js/lfm.js') }}"></script>
<script>
  $(document).ready( function () {
      $('#lfm').filemanager('image');
  });
</script>
@endsection